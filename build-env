#!/bin/bash
set -eo pipefail

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "$DIR/helpers.sh"
logname="build-env"

BINARY_TAG="$( "$DIR/config.py" binaryTag )"
USE_DOCKER="$( "$DIR/config.py" useDocker )"

USE_KERBEROS=true
if [ "$1" = "--no-kerberos" ]; then
    shift
    USE_KERBEROS=false
fi

if [ "$USE_DOCKER" != true ]; then
    HOST_OS=$(/cvmfs/lhcb.cern.ch/lib/bin/host_os)
    if [ "$HOST_OS" != x86_64-centos7 ]; then
        log ERROR "Host OS $HOST_OS is not supported. Find a CentOS 7 machine"
        log ERROR "or use docker by setting \"useDocker\" to true in ${DIR}/config.json"
        exit 1
    fi
elif [ "$USE_KERBEROS" = true ]; then
    # If using docker, we should obtain a tgt in a FILE: cache
    # TODO is there a way to forward a tgt from types other than FILE, e.g KCM?
    export KRB5CCNAME=$(klist 2>/dev/null | head -1 | grep -o 'FILE:.*')
    if [ -z "$KRB5CCNAME" ]; then
        log ERROR "No kerberos ticket cache of the form 'FILE:path/to/file' was found."
        log ERROR "Please do this to forward a kerberos ticket to the conainer:"
        log ERROR
        log ERROR "    export KRB5CCNAME='FILE:/tmp/krb5cc_$(id -u)'; kinit <username>@CERN.CH"
        log ERROR
        exit 1
    fi
fi

if [ "$USE_KERBEROS" = true ]; then
    # Check for a valid ticket
    if ! klist -s; then
        log ERROR "No valid ticket found. Please get a new one with"
        log ERROR
        log ERROR "    kinit <username>@CERN.CH"
        log ERROR
        exit 1
    fi
    kerberos_flag="--kerberos"
fi

if [ "$USE_DOCKER" = true ]; then
    "${DIR}/lb-docker-run" \
        --docker-tag v4.57 \
        --quiet-env --lbenv -c ${BINARY_TAG} \
        -C "${DIR}/.." --use-absolute-path $kerberos_flag \
        ${LB_DOCKER_RUN_FLAGS} \
        "$@"
else
    # Start a clean LbEnv a la lb-docker-run
    env -i "HOME=${HOME}" "HOSTNAME=${HOSTNAME}" "USER=${USER}" \
        "TERM=${TERM}" "KRB5CCNAME=${KRB5CCNAME}" \
        "BINARY_TAG=${BINARY_TAG}" \
        bash "${DIR}/native.sh" "$@"
fi
